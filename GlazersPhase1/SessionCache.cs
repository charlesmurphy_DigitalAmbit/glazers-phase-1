﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlazersPhase1.Apex;

namespace GlazersPhase1
{
    public class SessionCache
    {
        public long UserEntityKey { get; set; }
        public SessionHeader SessionHeader { get; set; }
        public string QueryServiceClientUrl { get; set; }
        public string DefaultSaveServiceClientUrl { get; set; }
        public List<CachedRegionData> CachedRegionStore { get; set; }

        public SessionCache()
        {
            SessionHeader = null;
            CachedRegionStore = new List<CachedRegionData>();
        }

        public void Reset()
        {
            SessionHeader = null;
            CachedRegionStore = new List<CachedRegionData>();
        }
    }
}
