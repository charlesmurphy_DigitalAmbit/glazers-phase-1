﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace GlazersPhase1
{
    public class Config
    {
        internal static readonly string RNALogin = ConfigurationManager.AppSettings["LoginEmail"];
        internal static readonly string RNAPassword = ConfigurationManager.AppSettings["LoginPassword"];
        internal static readonly string RNAAppIdentifier = ConfigurationManager.AppSettings["ClientApplicationIdentifier"];
        internal static readonly int SleepDuration = Convert.ToInt32(ConfigurationManager.AppSettings["SleepDuration"]);
        internal static readonly string LogFilePath = ConfigurationManager.AppSettings["LogFilePath"];
        internal static readonly string[] RegionFilter = ConfigurationManager.AppSettings["RegionFilter"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        internal static readonly int LookUpDate = Convert.ToInt16(ConfigurationManager.AppSettings["LookUpDate"]);
        internal static readonly string XMLSaveFileLocation = ConfigurationManager.AppSettings["XMLSaveLocation"];
        internal static readonly bool UseCache = Convert.ToBoolean(ConfigurationManager.AppSettings["UseCache"]);
        internal static readonly int DriverTypeId = Convert.ToInt32(ConfigurationManager.AppSettings["DriverTypeEntityKey"]);
        internal static readonly int RouteIdLength = Convert.ToInt32(ConfigurationManager.AppSettings["RouteIdLength"]);
    }
}
