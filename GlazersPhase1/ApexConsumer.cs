﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlazersPhase1.Apex;
using System.ServiceModel;
using log4net;

namespace GlazersPhase1
{
    class ApexConsumer
    {
        public QueryServiceClient QueryServiceClient = new QueryServiceClient();
        public log4net.ILog Logger;
        public SessionHeader SessionHeader = new SessionHeader();
        public SingleRegionContext RegionContext = new SingleRegionContext();

        public ApexConsumer(log4net.ILog logger, QueryServiceClient client, SessionHeader header, SingleRegionContext context)
        {
            Logger = logger;
            QueryServiceClient = client;
            SessionHeader = header;
            RegionContext = context;
        }

        public List<Worker> RetrieveDrivers(ref bool sessionExpired)
        {
            Logger.Debug("Begin Retrieving Drivers List");
            try
            {

                RetrievalResults drsRetrievalResults = QueryServiceClient.Retrieve(
                    SessionHeader,
                    RegionContext,
                    new RetrievalOptions
                    {
                        Expression = new EqualToExpression
                        {
                            Left = new PropertyExpression { Name = "WorkerTypeEntityKey" },
                            Right = new ValueExpression { Value = Config.DriverTypeId }
                        },
                        PropertyInclusionMode = PropertyInclusionMode.AccordingToPropertyOptions,
                        PropertyOptions = new WorkerPropertyOptions
                        {
                            Identifier = true,
                            
                        },
                        Type = "Worker"
                    });

                if (drsRetrievalResults.Items == null)
                {
                    Logger.Error("Retrieve Drivers Session Failed");

                }
                else if (drsRetrievalResults.Items.Length == 0)
                {
                    Logger.Error("No Drivers found ");
                    return null;
                }
                else
                {
                    
                    List<Worker> drivers = drsRetrievalResults.Items.Cast<Worker>().ToList<Worker>();
                    Logger.DebugFormat("Successfully Retrieved List of {0} Drivers.", drivers?.Count);
                    return drivers;
                    
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status == "SessionAuthenticationFailed" || tec.Detail.ErrorCode_Status == "InvalidEndpointRequest")
                {
                    sessionExpired = true;
                }
                else
                {
                    Logger.Error("Unknown FaultException: " + tec.Message);
                }
            }

            return null;

        }

        public List<DailyRoutingSession> RetrieveDailyRoutingSession(DateTime startDate, ref bool sessionExpired)
        {
            Logger.Debug("Begin Retrieving Daily Routing Sessions");
            try
            {

                RetrievalResults drsRetrievalResults = QueryServiceClient.Retrieve(
                    SessionHeader,
                    RegionContext,
                    new RetrievalOptions
                    {
                        Expression = new EqualToExpression
                        {
                            Left = new PropertyExpression { Name = "StartDate" },
                            Right = new ValueExpression { Value = startDate }
                        },
                        PropertyInclusionMode = PropertyInclusionMode.All,
                        Type = "DailyRoutingSession"

                        
                    });

                if (drsRetrievalResults.Items == null)
                {
                    Logger.Error("Retrieve Daily Routing Session Failed");

                }
                else if (drsRetrievalResults.Items.Length == 0)
                {
                    Logger.Error("No Daily Routing Sessions exist for " + startDate.ToShortDateString() + " | ");
                    return null;
                }
                else
                {
                    Logger.Debug("Retrieve Daily Routing Session completed successfully.");
                    List<DailyRoutingSession> dailyRoutingSessions = drsRetrievalResults.Items.Cast<DailyRoutingSession>().ToList();
                    return dailyRoutingSessions;

                    

                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status == "SessionAuthenticationFailed" || tec.Detail.ErrorCode_Status == "InvalidEndpointRequest")
                {
                    sessionExpired = true;
                }
                else
                {
                    Logger.Error("Unknown FaultException: " + tec.Message);
                }
            }

            return null;

        }

        public List<Route> RetrieveRoutes(long routingSessionEntityKey, ref bool sessionExpired)
        {
            Logger.Debug("Begin Retrieve Routes process.");



            try
            {
                RetrievalResults retrieveRoutes = QueryServiceClient.Retrieve(
                    SessionHeader,
                    RegionContext,
                    new RetrievalOptions
                    {
                        Expression = new AndExpression
                        {
                            Expressions = new SimpleExpressionBase[]
                            {
                                new EqualToExpression
                                {
                                    Left = new PropertyExpression { Name = "RoutingSessionEntityKey" },
                                    Right = new ValueExpression { Value = routingSessionEntityKey }

                                },
                                new EqualToExpression
                                {
                                    Left = new PropertyExpression{ Name ="RouteState_State"},
                                    Right = new ValueExpression { Value = "Completed"}
                                },
                                new EqualToExpression
                                {
                                    Left = new PropertyExpression{ Name ="RoutePhase_Phase"},
                                    Right = new ValueExpression { Value = "Dispatch"}
                                }

                            }

                        },

                        PropertyInclusionMode = PropertyInclusionMode.AccordingToPropertyOptions,
                        PropertyOptions = new RoutePropertyOptions
                        {
                            WorkersOptions = new RouteWorkerPropertyOptions
                            {
                                WorkerIdentifier = true,
                                WorkerEntityKey = true
                            },
                            DestinationDepotIdentifier = true,
                            Stops = true,
                            Workers = true,
                            Identifier = true,
                            DepartureTime = true,
                            PlannedNumberOfServiceableStops = true,
                            PlannedNumberOfNonServiceableStops = true,
                            NumberOfNonServiceableStops = true,
                            NumberOfServiceableStops = true,
                            PlannedDepartureTime = true,
                            PlannedArrivalTime = true,
                            ArrivalTime = true,
                            TotalPlannedTime = true,
                            TotalTime = true,
                            TotalPlannedDistance = true,
                            TotalDistance = true,
                            MaxRunningQuantity = true,
                            TotalDeliveryQuantities = true,
                            MobileDeviceIdentifier = true,
                            CompleteTime = true,
                            Capacity = true,
                            NumberOfOrders = true,
                            DestinationDepotEntityKey = true,
                            
                            

                            StopsOptions = new StopPropertyOptions
                            {
                                ArrivalTime = true,
                                Actions = true,
                                ServiceLocationIdentifier = true,
                                PlannedSequenceNumber = true,
                                SequenceNumber = true,
                                PlannedArrivalTime = true,
                                PlannedDepartureTime = true,
                                DepartureTime = true,
                                TotalPlannedServiceTime = true,
                                TotalServiceTime = true,
                                Coordinate = true,
                                GeocodeAccuracy_ServiceLocationGeocodeAccuracy = true,
                                MissedTimeWindowDuration = true,

                                ActionsOptions = new StopActionPropertyOptions
                                {
                                    OrderIdentifier = true,
                                    OrderServiceWindowDetails = true,
                                    Quantities = true,
                                    PlannedQuantities = true,
                                    OrderState_OrderState = true,
                                

                                },
                            }
                        },

                        Type = "Route"
                    });

                if (retrieveRoutes.Items == null)
                {
                    Logger.ErrorFormat("Retrieve Routes for Region {0} failed.", RegionContext.RegionEntityKey.ToString());
                }
                else if (retrieveRoutes.Items.Length == 0)
                {
                    Logger.Error("No routes exist for Region " + RegionContext.RegionEntityKey.ToString());
                }
                else
                {
                    List<Route> routesForSession = retrieveRoutes.Items.Cast<Route>().ToList();
                    Logger.DebugFormat("Retrieve {0} Routes for Region {1} completed successfully.", routesForSession?.Count, RegionContext.RegionEntityKey.ToString());
                    return routesForSession;
                }

            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status == "SessionAuthenticationFailed" || tec.Detail.ErrorCode_Status == "InvalidEndpointRequest")
                {
                    sessionExpired = true;
                }
                else
                {
                    Logger.Error("Unknown FaultException: " + tec.Message);
                }
            }

            return null;

        }
    }
}
