﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ServiceModel;
using GlazersPhase1.Apex;
using System.Xml;
using System.Xml.Linq;

namespace GlazersPhase1
{
    class Program
    {
        static log4net.ILog Logger;
        static SessionCache SessionData;
        static string cacheFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RoadnetAnywhereSession.json");

        static QueryServiceClient QueryServiceClient;
        static RoutingServiceClient DefaultSaveServiceClient;

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Logger = CreateRollingFileLogger("GlazersBeverageIntegration");
            SessionData = new SessionCache();

            bool completedSuccessfully = false;
            while (!completedSuccessfully)
            {
                if (Config.UseCache == false && File.Exists(cacheFile))
                {
                    File.Delete(cacheFile);
                }
                LoadCachedUrls();
                
                if (SessionData.SessionHeader == null )
                {
                    Login();
                    if (SessionData.SessionHeader != null)
                    {
                        Region[] regions = RetrieveRegions();
                        foreach (Region region in regions)
                        {
                            // if region is in the configured list of regions to process here
                            // then get URLS for it.
                            if (Config.RegionFilter.Contains(region.Identifier.ToUpper()))
                            {
                                SingleRegionContext context = new SingleRegionContext
                                {
                                    RegionEntityKey = region.EntityKey,
                                    BusinessUnitEntityKey = region.BusinessUnitEntityKey
                                };

                                UrlSet regionUrls = RetrieveUrlSetByRegionContext(context, region.EntityKey.ToString());
                                if (regionUrls != null)
                                {
                                    SessionData.CachedRegionStore.Add(new CachedRegionData
                                    {
                                        regionContext = context,
                                        urlSet = regionUrls
                                    });
                                }
                            }
                        }
                        WriteCachedUrls();
                    }
                }

                if (SessionData.SessionHeader != null && SessionData.CachedRegionStore != null)
                {
                    /*Code start*/
                    bool errorEncountered = false;

                    DateTime LookUpDate = DateTime.Today.AddDays(Config.LookUpDate);


                    foreach (CachedRegionData regionData in SessionData.CachedRegionStore)
                    {

                        string formatDouble62 = "000000.00";
                        string formatDouble82 = "00000000.00";
                        string formatTime = "yyyyMMddHHmmss";
                        string formatTimeFilename = "yyyyMMddHHmmss";
                        string latLongFormat = "00.000000";
                        TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");



                        bool sessionExpired = false;
                        if (sessionExpired)
                        {
                            Logger.Debug("Session Expired after Retreive Daily Routing Sessions");
                            errorEncountered = true;
                            Logger.Debug("Starting to Reset Session Cache");
                            ResetSessionCache();
                            Logger.Debug("Session Cache Has Been Deleted And Reset.");
                            continue;
                        }

                        ApexConsumer _Consumer = new ApexConsumer(Logger, QueryServiceClient, SessionData.SessionHeader, regionData.regionContext);
                        List<DailyRoutingSession> sessions = _Consumer.RetrieveDailyRoutingSession(LookUpDate, ref sessionExpired);
                        List<Worker> driversInRegion = _Consumer.RetrieveDrivers(ref sessionExpired);
                        if (sessionExpired) 
                        {
                            Logger.Debug("Session Expired after Retreive Daily Routing Sessions");
                            errorEncountered = true;
                            Logger.Debug("Starting to Reset Session Cache");
                            ResetSessionCache();
                            Logger.Debug("Session Cache Has Been Deleted And Reset.");
                            continue;
                        }
                        if (sessions == null)
                        {
                            errorEncountered = true;
                        }

                        else
                        {
                            foreach (DailyRoutingSession dailyRoutingSession in sessions)
                            {
                                if (sessionExpired)
                                {
                                    continue;
                                }

                                Logger.Info("Daily Routing Session: " + dailyRoutingSession.Description + " | " + dailyRoutingSession.SessionMode_Mode + " | " + dailyRoutingSession.StartDate + " | Region: " + regionData.regionContext.RegionEntityKey.ToString());

                                string routingSessionDate = dailyRoutingSession.StartDate;
                                

                                List<Route> routes = _Consumer.RetrieveRoutes(dailyRoutingSession.EntityKey, ref sessionExpired);
                              


                                if (sessionExpired)
                                {
                                    errorEncountered = true;

                                    continue;
                                }
                                if (routes == null)
                                {
                                    errorEncountered = true;
                                }
                                else
                                {
                                    //Do the Things for the file stuff here or create a class for that



                                    //var frequency = routes.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());


                                    Logger.InfoFormat("Retrieving Routes with and ID length less than  {0} for Region {1}", Config.RouteIdLength.ToString(), regionData.regionContext.RegionEntityKey.ToString());
                                    foreach (Route route in routes)
                                        {
                                            XDocument exportDocument = null;
                                            XElement deliveryOrderXml = null;
                                            string filename = String.Empty;
                                            string driverId = String.Empty;
                                            string routeId = String.Empty;
                                            string warehouseId = String.Empty;
                                            string deliveryDate = String.Empty;
                                            string plannedNumberOfTotalStops = String.Empty;
                                            string actualNumberofTotalStops = String.Empty;
                                            string routePlannedDepartureTime = String.Empty;
                                            string routeActualDepartureTime = String.Empty;
                                            string routePlannedArrivalTime = String.Empty;
                                            string routeArrivalTime = String.Empty;
                                            string routeTotalPlannedTime = String.Empty;
                                            string routeTotalTime = String.Empty;
                                            string totalPlannedDistance = String.Empty;
                                            string totalActualDistance = String.Empty;
                                            string maxRunningQuantity = String.Empty;
                                            string totalDeliveryQuantities = String.Empty;
                                            string mobileDeviceIdentifier = String.Empty;
                                            string maxWeight = String.Empty;
                                            string actualWeight = String.Empty;
                                            string weightPercentage = String.Empty;
                                            int deliveryCountNumber = 0;
                                            DateTime stopArrivalTime = new DateTime();
                                            string DeliveryOrderId = String.Empty;
                                            string thisStopPlannedSequenceNumber = String.Empty;
                                            string thisStopSequenceNumber = String.Empty;
                                            string accountId = String.Empty;
                                            string thisStopPlannedArrivalTime = String.Empty;
                                            string thisStopArrivalTime = String.Empty;
                                            string thisStopPlannedDepartureTime = String.Empty;
                                            string thisStopDepartureTime = String.Empty;
                                            string thisStopTotalPlannedServiceTime = String.Empty;
                                            string thisStopTotalServiceTime = String.Empty;
                                            string thisStopLat = String.Empty;
                                            string thisStopLong = String.Empty;
                                            string thisStopGeocodeAccuracy = String.Empty;
                                            string[] deliveryOrderTimeWindowStart = new string[2];
                                            string[] deliveryOrderTimeWindowEnd = new string[2];
                                            char deliveryOrderTimeWindowMet;
                                            string deliveryOrderCasesDelivered = String.Empty;
                                            string deliveryOrderCasesNotDelivered = String.Empty;
                                            string deliveryOrderBottlesDelivered = String.Empty;
                                            string deliveryOrderBottlesNotDelivered = String.Empty;

                                            if (route.Identifier.Length <= Config.RouteIdLength)
                                            {

                                           
                                            if (route != null)
                                                {
                                                    try
                                                    {
                                                        //Non Nullable Values

                                                        driverId = String.Empty;
                                                        routeId = route.Identifier;
                                                        deliveryDate = TimeZoneInfo.ConvertTimeFromUtc(route.CompleteTime.Value.ToUniversalTime(), easternZone).ToString("yyyyMMdd");
                                                        routeArrivalTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(route.ArrivalTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                        routeTotalTime = String.Format("{0}{1}{2}00", route.TotalTime.Hours.ToString("00"), route.TotalTime.Minutes.ToString("00"), route.TotalTime.Seconds.ToString("00"));
                                                        totalActualDistance = route.TotalDistance.Value.ToString(formatDouble62);
                                                        maxRunningQuantity = route.MaxRunningQuantity.Size3.ToString(formatDouble82);
                                                        totalDeliveryQuantities = route.TotalDeliveryQuantities.Size3.ToString(formatDouble82);
                                                        mobileDeviceIdentifier = route.MobileDeviceIdentifier;
                                                        maxWeight = route.Capacity.Size2.ToString("000000.00");
                                                        actualWeight = route.TotalDeliveryQuantities.Size2.ToString("000000.00");
                                                        weightPercentage = ((route.TotalDeliveryQuantities.Size2 / route.Capacity.Size2)*100).ToString("00.00000");
                                                        routeActualDepartureTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(route.DepartureTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                        actualNumberofTotalStops = (route.NumberOfServiceableStops + route.NumberOfNonServiceableStops).ToString("000");

                                                        // Nullable Values that need to be accounted for

                                                        if (route.DestinationDepotIdentifier != null)
                                                        {
                                                            filename = String.Format(Config.XMLSaveFileLocation + "/{0}_{1}_{2}_{3}.xml", "DELIVERY_DATA_RN", route.DestinationDepotIdentifier.ToString(), route.Identifier, route.CompleteTime.Value.ToUniversalTime().ToString(formatTimeFilename));
                                                            warehouseId = route.DestinationDepotIdentifier.ToString();
                                                        }
                                                        else
                                                        {

                                                            filename = String.Format(Config.XMLSaveFileLocation + "/{0}_{1}_{2}_{3}.xml", "DELIVERY_DATA_RN", "DEPOT_ID_NOT_PROVIDED", route.Identifier, TimeZoneInfo.ConvertTimeFromUtc(route.CompleteTime.Value.ToUniversalTime(), easternZone).ToString(formatTimeFilename));
                                                            warehouseId = "UKNOWN";
                                                            Logger.ErrorFormat("DEBUG | Destination Depot Identifier Attribute is Null for route {0} ", routeId);
                                                        }




                                                        if (route.TotalPlannedDistance != null)
                                                        {
                                                            totalPlannedDistance = route.TotalPlannedDistance.Value.ToString(formatDouble62);
                                                        }
                                                        else
                                                        {
                                                            Logger.ErrorFormat("DEBUG | Total Planned Distance Attribute is null for route {0} ", routeId);
                                                            totalPlannedDistance = "000000.00";
                                                        }

                                                        if (route.PlannedNumberOfServiceableStops.HasValue != false || route.PlannedNumberOfNonServiceableStops.HasValue != false)
                                                        {
                                                            plannedNumberOfTotalStops = (route.PlannedNumberOfServiceableStops.Value + route.PlannedNumberOfNonServiceableStops.Value).ToString("000");
                                                        }
                                                        else
                                                        {
                                                            Logger.ErrorFormat("DEBUG | Either Planned Number of Servicable Stops Attribute or Either Planned Number of Non servicable Stops Attribute is null for route {0} ", routeId);
                                                            plannedNumberOfTotalStops = "0";
                                                        }



                                                        if (route.PlannedDepartureTime.HasValue != false)
                                                        {
                                                            routePlannedDepartureTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(route.PlannedDepartureTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                        }
                                                        else
                                                        {
                                                            Logger.ErrorFormat("DEBUG | Planned Departure Time Attribute is Null for route {0} ", routeId);
                                                            routePlannedDepartureTime = "000000000000000000";
                                                        }

                                                        if (route.TotalPlannedTime.HasValue != false)
                                                        {
                                                            routeTotalPlannedTime = String.Format("{0}{1}{2}00", route.TotalPlannedTime.Value.Hours.ToString("00"), route.TotalPlannedTime.Value.Minutes.ToString("00"), route.TotalPlannedTime.Value.Seconds.ToString("00"));
                                                        }
                                                        else
                                                        {
                                                            Logger.ErrorFormat("DEBUG | Total Planned Time Attribute is Null for route {0} ", routeId);
                                                            routeTotalPlannedTime = "00000000";
                                                        }



                                                        if (route.PlannedArrivalTime.HasValue != false)
                                                        {
                                                            routePlannedArrivalTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(route.PlannedArrivalTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                        }
                                                        else
                                                        {
                                                            Logger.ErrorFormat("DEBUG | Planned Arrival Time Attribute is Null for route {0} ", routeId);
                                                            routePlannedArrivalTime = "000000000000000000";
                                                        }

                                                        //Determine Driver Id for Route
                                                        foreach (RouteWorker workerOnRoute in route.Workers)
                                                        {
                                                            foreach (Worker driver in driversInRegion)
                                                            {

                                                                if (workerOnRoute.WorkerIdentifier == driver.Identifier)
                                                                {
                                                                    driverId = workerOnRoute.WorkerIdentifier;
                                                                }
                                                            }
                                                        }

                                                        //Write XML Document
                                                        exportDocument = new XDocument(
                                                                                new XDeclaration("1.0", "utf - 16", "true"),
                                                                                new XElement("DeliveryData",
                                                                                    new XElement("DeliveryHeader",
                                                                                        new XElement("FileSource", "RN"),
                                                                                        new XElement("Warehouse", warehouseId),
                                                                                        new XElement("DeliveryDate", deliveryDate),
                                                                                        new XElement("RouteCount", "1")),
                                                                                        new XElement("DeliveryRoute",
                                                                                            new XElement("RouteNumber", routeId.ToString()),
                                                                                            new XElement("DriverNumber", driverId),
                                                                                            new XElement("PlannedTotalStops", plannedNumberOfTotalStops),
                                                                                            new XElement("ActualTotalStops", actualNumberofTotalStops),
                                                                                            new XElement("PlannedDeparture", routePlannedDepartureTime),
                                                                                            new XElement("ActualDeparture", routeActualDepartureTime),
                                                                                            new XElement("PlannedArrival", routePlannedArrivalTime),
                                                                                            new XElement("ActualArrival", routeArrivalTime),
                                                                                            new XElement("TotalPlannedTime", routeTotalPlannedTime),
                                                                                            new XElement("TotalActualTime", routeTotalTime),
                                                                                            new XElement("PlannedDistance", totalPlannedDistance),
                                                                                            new XElement("ActualDistance", totalActualDistance),
                                                                                            new XElement("MaximumWeight", maxWeight),
                                                                                            new XElement("ActualWeight", actualWeight),
                                                                                            new XElement("WeightPercent", weightPercentage),
                                                                                            new XElement("WirelessID", mobileDeviceIdentifier),
                                                                                            new XElement("DeliveryCount", "Placeholder")
                                                                                            )));
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Logger.ErrorFormat("{0}", ex.Message);
                                                    }


                                                }

                                                if (route.Stops != null)
                                                {



                                                    foreach (Apex.Stop stop in route.Stops)
                                                    {

                                                        ServiceableStop thisStop = new ServiceableStop();




                                                        if (stop is ServiceableStop)
                                                        {



                                                            try
                                                            {
                                                                //Cast stop as servicable stop if it is servicable to get other values
                                                                thisStop = (ServiceableStop)stop;
                                                                // Non-Nullable Values 

                                                                accountId = thisStop.ServiceLocationIdentifier;



                                                                thisStopLat = (thisStop.Coordinate.Latitude * 0.000001).ToString(latLongFormat);
                                                                thisStopLong = (thisStop.Coordinate.Longitude * 0.000001).ToString(latLongFormat);
                                                                thisStopGeocodeAccuracy = thisStop.GeocodeAccuracy_ServiceLocationGeocodeAccuracy;

                                                                // Nullable Values that need to be accounted for
                                                                if (thisStop.TotalServiceTime.HasValue != false)
                                                                {
                                                                    thisStopTotalServiceTime = String.Format("{0}{1}{2}00", thisStop.TotalServiceTime.Value.Hours.ToString("00"), thisStop.TotalServiceTime.Value.Minutes.ToString("00"), thisStop.TotalServiceTime.Value.Seconds.ToString("00"));
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Stop Total Service Time attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopTotalServiceTime = "00000000";
                                                                }
                                                                if (thisStop.DepartureTime != null)
                                                                {
                                                                    thisStopDepartureTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(thisStop.DepartureTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Stop Departure Time Attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopDepartureTime = "000000000000000000";
                                                                }

                                                                if (thisStop.ArrivalTime != null)
                                                                {
                                                                    thisStopArrivalTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(thisStop.ArrivalTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Stop Arrival Time Attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopArrivalTime = "000000000000000000";
                                                                }

                                                                if (thisStop.SequenceNumber.HasValue != false)
                                                                {
                                                                    thisStopSequenceNumber = thisStop.SequenceNumber.Value.ToString("000");
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Stop Sequence Number Attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopSequenceNumber = "000";
                                                                }


                                                                if (thisStop.PlannedSequenceNumber.HasValue != false)
                                                                {
                                                                    thisStopPlannedSequenceNumber = thisStop.PlannedSequenceNumber.Value.ToString("000");
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Stop Planned Sequence Number Attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopPlannedSequenceNumber = "000";
                                                                }

                                                                if (thisStop.PlannedArrivalTime.HasValue != false)
                                                                {
                                                                    thisStopPlannedArrivalTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(thisStop.PlannedArrivalTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Planned Stop Arrival Time Attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopPlannedArrivalTime = "000000000000000000";
                                                                }


                                                                if (thisStop.PlannedDepartureTime.HasValue != false)
                                                                {

                                                                    thisStopPlannedDepartureTime = String.Format("{0}00", TimeZoneInfo.ConvertTimeFromUtc(thisStop.PlannedDepartureTime.Value.ToUniversalTime(), easternZone).ToString(formatTime));
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Planned Stop Departure Time Attributeis Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopPlannedDepartureTime = "000000000000000000";
                                                                }

                                                                if (thisStop.TotalPlannedServiceTime.HasValue != false)
                                                                {
                                                                    thisStopTotalPlannedServiceTime = String.Format("{0}{1}{2}00", thisStop.TotalPlannedServiceTime.Value.Hours.ToString("00"), thisStop.TotalPlannedServiceTime.Value.Minutes.ToString("00"), thisStop.TotalPlannedServiceTime.Value.Seconds.ToString("00"));
                                                                }
                                                                else
                                                                {
                                                                    Logger.ErrorFormat("DEBUG | Planned Stop Service Time Attribute is Null for route {0}, stop {1} ", routeId, thisStop.ServiceLocationIdentifier);
                                                                    thisStopTotalPlannedServiceTime = "000000";
                                                                }

                                                                // Special way of getting TimeWindow Values 

                                                                deliveryOrderTimeWindowMet = 'U';
                                                                if (thisStop.MissedTimeWindowDuration.HasValue != false)
                                                                {
                                                                    if (thisStop.MissedTimeWindowDuration.Value.TotalHours != 0)
                                                                    {
                                                                        deliveryOrderTimeWindowMet = 'N';
                                                                    }
                                                                    else
                                                                    {
                                                                        deliveryOrderTimeWindowMet = 'Y';
                                                                    }
                                                                }

                                                                if ((thisStop.ArrivalTime != null) && (thisStop.ArrivalTime.Value != null))
                                                                {

                                                                    stopArrivalTime = thisStop.ArrivalTime.Value;



                                                                    if (thisStop.Actions != null)
                                                                    {

                                                                        foreach (StopAction action in thisStop.Actions)
                                                                        {
                                                                            if (action.OrderState_OrderState == "Serviced")
                                                                            {
                                                                                deliveryCountNumber++;
                                                                                // Non-Nullable Values 
                                                                                DeliveryOrderId = action.OrderIdentifier;
                                                                                deliveryOrderCasesDelivered = action.Quantities.Size1.ToString();
                                                                                deliveryOrderCasesNotDelivered = (action.PlannedQuantities.Size1 - action.Quantities.Size1).ToString();
                                                                                deliveryOrderBottlesDelivered = action.Quantities.Size3.ToString();
                                                                                deliveryOrderBottlesNotDelivered = (action.PlannedQuantities.Size3 - action.Quantities.Size3).ToString();



                                                                                var i = 0;
                                                                                if (action.OrderServiceWindowDetails != null || action.OrderServiceWindowDetails.Length != 0)
                                                                                {

                                                                                    foreach (OrderServiceWindowDetail windowDetail in action.OrderServiceWindowDetails)
                                                                                    {
                                                                                        if (windowDetail != null)
                                                                                        {
                                                                                            DateTime temp1, temp2;
                                                                                            TimeSpan tempTime1, tempTime2;
                                                                                            temp1 = thisStop.ArrivalTime.Value;
                                                                                            temp2 = thisStop.ArrivalTime.Value;

                                                                                            tempTime1 = new TimeSpan(Convert.ToInt32(windowDetail.DailyTimePeriod.StartTime.Substring(0, 2)), Convert.ToInt32(windowDetail.DailyTimePeriod.StartTime.Substring(3, 2)), Convert.ToInt32(windowDetail.DailyTimePeriod.StartTime.Substring(6, 2)));
                                                                                            tempTime2 = new TimeSpan(Convert.ToInt32(windowDetail.DailyTimePeriod.EndTime.Substring(0, 2)), Convert.ToInt32(windowDetail.DailyTimePeriod.EndTime.Substring(3, 2)), Convert.ToInt32(windowDetail.DailyTimePeriod.EndTime.Substring(6, 2)));

                                                                                            temp1 = temp1.Date + tempTime1;
                                                                                            temp2 = temp2.Date + tempTime2;

                                                                                            deliveryOrderTimeWindowStart[i] = String.Format("{0}00", temp1.ToString(formatTime));
                                                                                            deliveryOrderTimeWindowEnd[i] = String.Format("{0}00", temp2.ToString(formatTime));

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            deliveryOrderTimeWindowStart[i] = string.Empty;
                                                                                            deliveryOrderTimeWindowEnd[i] = string.Empty;
                                                                                        }

                                                                                        i++;


                                                                                    }

                                                                                    if ((deliveryOrderTimeWindowStart[0] == null && deliveryOrderTimeWindowStart[1] == null) || (deliveryOrderTimeWindowStart[0] == string.Empty && deliveryOrderTimeWindowStart[1] == string.Empty))
                                                                                    {
                                                                                        deliveryOrderTimeWindowMet = 'U';
                                                                                    }


                                                                                }




                                                                                //Write DeliveryOrderFile
                                                                                deliveryOrderXml = new XElement("DeliveryOrder",
                                                                                                        new XElement("AccountID", accountId),
                                                                                                        new XElement("OrderID", DeliveryOrderId),
                                                                                                        new XElement("PlanSequence", thisStopPlannedSequenceNumber),
                                                                                                        new XElement("ActualSequence", thisStopSequenceNumber),
                                                                                                        new XElement("TimeWindow1Start", deliveryOrderTimeWindowStart[0]),
                                                                                                        new XElement("TimeWindow1End", deliveryOrderTimeWindowEnd[0]),
                                                                                                        new XElement("TimeWindow2Start", deliveryOrderTimeWindowStart[1]),
                                                                                                        new XElement("TimeWindow2End", deliveryOrderTimeWindowEnd[1]),
                                                                                                        new XElement("PlannedArrivalTime", thisStopPlannedArrivalTime),
                                                                                                        new XElement("PlannedDepartureTime", thisStopPlannedDepartureTime),
                                                                                                        new XElement("ActualArrivalTime", thisStopArrivalTime),
                                                                                                        new XElement("ActualDepartureTime", thisStopDepartureTime),
                                                                                                        new XElement("PlannedServiceTime", thisStopTotalPlannedServiceTime),
                                                                                                        new XElement("ActualServiceTime", thisStopTotalServiceTime),
                                                                                                        new XElement("TimeWindowMet", deliveryOrderTimeWindowMet),
                                                                                                        new XElement("DeliveryLatitude", thisStopLat),
                                                                                                        new XElement("DeliveryLongitude", thisStopLong),
                                                                                                        new XElement("CasesDelivered", deliveryOrderCasesDelivered),
                                                                                                        new XElement("CasesNotDelivered", deliveryOrderCasesNotDelivered),
                                                                                                        new XElement("BottlesDelivered", deliveryOrderBottlesDelivered),
                                                                                                        new XElement("BottlesNotDelivered", deliveryOrderBottlesNotDelivered),
                                                                                                        new XElement("DataQuality", "GOOD"),
                                                                                                        new XElement("LocationGeoCode", thisStopGeocodeAccuracy)
                                                                                                        );



                                                                                exportDocument.Element("DeliveryData").Element("DeliveryRoute").Add(deliveryOrderXml);



                                                                            }

                                                                        }
                                                                    }


                                                                }



                                                            }


                                                            catch (Exception ex)
                                                            {
                                                                Logger.ErrorFormat("Error getting values: {0}", ex.Message);
                                                            }


                                                        }


                                                    }


                                                }


                                                try
                                                {

                                                    XElement xmlChild = exportDocument.Element("DeliveryData").Element("DeliveryRoute").Element("DeliveryCount");

                                                    xmlChild.ReplaceWith(new XElement("DeliveryCount", deliveryCountNumber.ToString("000000")));

                                                    ForceTags(exportDocument).Save(filename);
                                                   

                                                //exportDocument.Save(filename);
                                            }
                                                catch (Exception ex)
                                                {
                                                    Logger.ErrorFormat("{0}", ex.Message);
                                                }



                                            }
                                        }
                                    Logger.InfoFormat("Retrieved {0} Routes with and ID length less than {1} for Region {2} completed successfully", routes?.Where(x=>x.Identifier.Length <= Config.RouteIdLength).Count(), Config.RouteIdLength.ToString(), regionData.regionContext.RegionEntityKey.ToString());






                                }

                            }
                        }





                    }
                    if (!errorEncountered)
                    {
                        completedSuccessfully = true;
                    }

                }

                if (!completedSuccessfully)
                {
                    Logger.WarnFormat("Reached end of processing without completing. Pausing for {0} seconds before repeating process.", Config.SleepDuration / 1000);
                    System.Threading.Thread.Sleep(Config.SleepDuration);
                }
            }
        }

        static void LoadCachedUrls()
        {
            if (!File.Exists(cacheFile))
            {
                Logger.Info("No cache file exists");
            }
            else
            {
                Logger.InfoFormat("Loading cache file from {0}", cacheFile);
                try
                {
                    string jsonData = File.ReadAllText(cacheFile);
                    SessionCache cachedData = Newtonsoft.Json.JsonConvert.DeserializeObject<SessionCache>(jsonData);
                    if (cachedData != null)
                    {
                        SessionData = cachedData;
                        DefaultSaveServiceClient = new RoutingServiceClient("BasicHttpBinding_IRoutingService", SessionData.DefaultSaveServiceClientUrl);
                        QueryServiceClient = new QueryServiceClient("BasicHttpBinding_IQueryService", SessionData.QueryServiceClientUrl);
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Error opening cache file: {0}", ex.Message);
                }
            }
        }

        static void WriteCachedUrls()
        {
            Logger.InfoFormat("Writing cache file to {0}", cacheFile);
            try
            {
                using (StreamWriter writer = new StreamWriter(cacheFile, append: false))
                {
                    Newtonsoft.Json.JsonSerializerSettings settings = new Newtonsoft.Json.JsonSerializerSettings
                    {
                        PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None,
                        ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    };

                    string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(SessionData, Newtonsoft.Json.Formatting.None, settings);

                    writer.Write(jsonData);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error writing cache file: {0}", ex.Message);
            }
        }

        static void ResetSessionCache()
        {
            SessionData.Reset();
            Logger.InfoFormat("Removing cache file at {0}", cacheFile);
            try
            {
                File.Delete(cacheFile);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error deleteing cache file: {0}", ex.Message);
            }
        }

        static void Login()
        {
            try
            {
                Logger.Debug("Begin Login process.");

                LoginServiceClient loginServiceClient = new LoginServiceClient();
                LoginResult loginResult = loginServiceClient.Login(
                    Config.RNALogin,
                    Config.RNAPassword,
                    new CultureOptions(),
                    new ClientApplicationInfo
                    {
                        ClientApplicationIdentifier = new Guid(Config.RNAAppIdentifier)
                    });

                if (loginResult == null)
                {
                    Logger.Error("Login failed.");
                }
                else
                {
                    Logger.Debug("Login completed successfully for user " + Config.RNALogin.ToString() + " User EntityKey: " + loginResult.User.EntityKey);

                    SessionData.UserEntityKey = loginResult.User.EntityKey;

                    Logger.Debug("Storing SessionHeader: " + loginResult.UserSession.Guid);
                    SessionData.SessionHeader = new SessionHeader { SessionGuid = loginResult.UserSession.Guid };

                    Logger.Debug("Create QueryServiceClient for: " + loginResult.QueryServiceUrl);
                    SessionData.QueryServiceClientUrl = loginResult.QueryServiceUrl;
                    QueryServiceClient = new QueryServiceClient("BasicHttpBinding_IQueryService", loginResult.QueryServiceUrl);

                    Logger.Debug("Create DefaultRoutingServiceClient for: " + loginResult.DefaultRoutingServiceUrl);
                    SessionData.DefaultSaveServiceClientUrl = loginResult.DefaultRoutingServiceUrl;
                    DefaultSaveServiceClient = new RoutingServiceClient("BasicHttpBinding_IRoutingService", loginResult.DefaultRoutingServiceUrl);


                    Logger.Debug("Login process complete.");
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                Logger.Error("TransferErrorCode caught: " + tec.Action + " | " + tec.Code.Name + " | " + tec.Detail.ErrorCode_Status + " | " + tec.Message);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        static Region[] RetrieveRegions()
        {
            try
            {
                Logger.Debug("Begin Retrieve Regions process.");

                RetrievalResults retrievalResults = QueryServiceClient.RetrieveRegionsGrantingPermissions(
                    SessionData.SessionHeader,
                    new RolePermission[] {  },
                    false);
                                
                if (retrievalResults.Items == null)
                {
                    Logger.Error("Retrieve Regions failed.");
                }
                else if (retrievalResults.Items.Length == 0)
                {
                    Logger.Error("No Regions exist.");
                }
                else
                {
                    Logger.Debug("Retrieve Regions completed successfully.");
                    return retrievalResults.Items.Cast<Region>().ToArray();
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status == "SessionAuthenticationFailed" || tec.Detail.ErrorCode_Status == "InvalidEndpointRequest")
                {
                    Logger.Warn("Resetting Session Cache");
                    ResetSessionCache();
                }
                else
                {
                    Logger.Error("TransferErrorCode caught: " + tec.Action + " | " + tec.Code.Name + " | " + tec.Detail.ErrorCode_Status + " | " + tec.Message, tec);
                    throw;
                }
            }
            catch (EndpointNotFoundException nfex)
            {
                Logger.Warn("Endpoint not found. Resetting Session Cache");
                ResetSessionCache();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
            return null;
        }

        static UrlSet RetrieveUrlSetByRegionContext(RegionContext context, string regionEntityKey)
        {
            UrlSet urlSet = null;
            try
            {
                Logger.Debug("Begin Retrieve Urls process for the Region with Entity Key: " + regionEntityKey);
                urlSet = QueryServiceClient.RetrieveUrlsForContext(SessionData.SessionHeader, context);
                Logger.Debug("Retrieve Urls process completed successfully for the Region with Entity Key: " + regionEntityKey);
                
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                Logger.Error("TransferErrorCode caught: " + tec.Action + " | " + tec.Code.Name + " | " + tec.Detail.ErrorCode_Status + " | " + tec.Message);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            return urlSet;
        }

        static log4net.ILog CreateRollingFileLogger(string logName)
        {
            log4net.Layout.PatternLayout layout = new log4net.Layout.PatternLayout("%date | %-5level | %message%newline");
            layout.ActivateOptions();
            log4net.Appender.RollingFileAppender appender = new log4net.Appender.RollingFileAppender();
            appender.Name = "RollingFileAppender_" + logName;
            appender.File = Config.LogFilePath + logName + "_.txt";
            appender.LockingModel = new log4net.Appender.FileAppender.MinimalLock();
            appender.RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Date;
            appender.DatePattern = "yyyyMMdd";
            appender.AppendToFile = true;
            appender.StaticLogFileName = false;
            appender.PreserveLogFileNameExtension = true;
            appender.Layout = layout;
            appender.ActivateOptions();
            log4net.ILog log = log4net.LogManager.GetLogger(logName);
            log4net.Repository.Hierarchy.Logger logger = (log4net.Repository.Hierarchy.Logger)log.Logger;
            logger.AddAppender(appender);
            return log;
        }

        // Note: This will mutate the specified document.
        static XDocument ForceTags(XDocument document)
        {
            foreach (XElement childElement in
                from x in document.DescendantNodes().OfType<XElement>()
                where x.IsEmpty
                select x)
            {
                childElement.Value = string.Empty;
            }

            return document;
        }
    }
}
